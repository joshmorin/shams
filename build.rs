extern crate gcc;

fn main() {
    use gcc;
    gcc::compile_library("libiprayertime.a", &["src/itl/prayer.c", "src/itl/astro.c"]);
    println!("cargo:rustc-link-lib=m");
}

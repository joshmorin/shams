/*
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
    TODO            
        - Correct tempreture for ShamsYear
            - issue: 
                    At present, the temprature is taken to be 
                    constant through-out the year. This might 
                    be trevial to fix if "good-enough" 
                    approximates are acceptable.
            - posible fixes:
                    Haven't researched this yet, but there 
                    might be a mathematical model I can use
                    to aproximate the value based on lat, lon,
                    and time of year. This solution will be 
                    best (independant of location).
                    
                    Another solution, if it generates passable
                    values is to store the min and max, then 
                    use a trigometric function with the base as
                    the the delta, and shifted by the minimum.
                    

*/


//! Islamic prayer times and other solar temporal events
//!
//! POSIX Time and UTC
//! ------------------
//! This library uses the POSIX/UNIX time system for representing 
//! time. This was chosen to simplify working with timestamps and 
//! durations - for both myself and the user - by deferring the 
//! handling of leap seconds and synchronization with UTC to the host
//! system.
//!
//! The UNIX timestamp is defined as the number of seconds that 
//! have elapsed since the epoch (1970-01-01T00:00:00Z - Note 
//! the timezone), not counting the leap seconds. As a corollary, 
//! this library does not support dates that predate the epoch, 
//! and the user is responsible for timezone and DST adjustments.
//!
//! For most use cases, the distinction between UTC and POSIX
//! time is not significant enough to warrant consideration.
//! 
//!
//! Extreme Methods
//! ---------------
//! In locations towards the north and south poles, the sun may 
//! remain visible multiple days, Extreme methods are the methods 
//! that can be used to approximate (or in some cases, define) 
//! some values returned by this library. Any value that cannot 
//! be calculated in such a situation should return `None`.
//!
//! Extreme methods need to be set to take affect.
//!
//!
//! # Copyright #
//! Copyright © 2015  Josh Morin <JoshMorin@gmx.com>
//! 
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Lesser General Public License as published by
//! the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//! 
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Lesser General Public License for more details.
//! 
//! You should have received a copy of the GNU General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate time;
extern crate libc;

mod itl;
mod db;
mod date;
mod tests;

pub mod examples;
pub use date::{Date, Time, Duration};
    
use libc::types::os::arch::c95::{c_int, c_double};



/// Environment and Calculation Specification.
///
/// This structure specifies the necessary details for generating
/// correct times of interest.
/// 
/// 1. **Environment Details**
///     1. Latitude and Longitude 
///     2. Elevation
///     3. Atmospheric Pressure
///     4. Temperature
///    
/// 2. **Time Details**
///     1. Prayer time Calculation Methods
///     2. Date
/// 
/// 3. **Extended Method Details**
/// 
/// Unless this structure is populated with all the necessary 
/// information calculations require, calculations will 
/// yield incorrect results. The `new` function does not 
/// initialize the specification to a usable state. Setting
/// information is done by call the appropriate method.
/// 
/// ###Extended Details###
///
/// Atmospheric pressure varies with elevation, location, temperature,
/// and time of year. For the eastern coast of Arabia, pressure can 
/// range from <`1000` millibars up to around `1020`. The standard 
/// value is `1010.0`.
///
/// Elevation, temperature, and location coordinates can all be 
/// determined with a quick web search.
///
/// Which method to use varies from one region to another. If you 
/// don't care about the correctness of prayer times, you can set 
/// it to 0.
///
/// [1]: https://commons.wikimedia.org/wiki/File:Mslp-jja-djf.png
#[derive(Default, Clone, Copy)]
pub struct ShamsSpec {
    method      : itl::Method,
    location    : itl::Location,
    date        : itl::Date
}
impl ShamsSpec {
    
    /// Empty ShamsSpec Object. This object must be converted to 
    /// a ShamsDay object by calling `calculate` or `year` before it 
    /// can be used to infer times.
    /// 
    /// Converting this object without specifying all the required 
    /// information will yield incorrect results.
    pub fn new() -> ShamsSpec {
        use std::default::Default;
        let mut s: ShamsSpec = Default::default();
        s.method(0);
        s
    }
    
    /// Attempts to set method, latitude, longitude, elevation, and 
    /// pressure/. This function's behaviour deviates from the 
    /// builder pattern because only a small set of cities have 
    /// these values defined.
    ///
    /// City names may be supplied in Arabic or English. If multiple 
    /// cities with the same name exist, include the name of the 
    /// country. For Example, the strings "Al-Quds", 
    /// "Al-Quds, Palestine", "القدس", and "القدس، فلسطين" are all
    /// equivalent and will set the same value. 
    ///
    /// Current cities defined are:
    ///
    /// 1. Makkah, Saudi Arabia
    /// 2. Medina, Saudi Arabia
    /// 3. Al-Quds, Palestine
    /// 4. Jakarta, Indonesia
    /// 5. Bandar Seri Begawan, Brunei
    /// 6. Kuwait, Kuwait
    ///
    /// If your city is not included in this list, please consider 
    /// taking the time to submit the necessary information to the 
    /// bug tracker. Thank you.
    pub fn city(name: &str) -> Option<ShamsSpec> {
        use db;
        let slow: Vec<&str> = name
            .split(|c| c == ',' || c == ' ' || c == '،')
            .filter(|s| s.len() != 0).collect();
            
        match db::CITIES.iter().find(|n| &n.0[..slow.len()] == &slow[..]) {
            Some(s) => {
                let mut d = ShamsSpec::new();
                d.method(s.1.method)
                 .coordinates(s.1.lat, s.1.lon)
                 .temperature((s.1.temp.0+s.1.temp.1)/2.0)
                 .pressure(s.1.pres)
                 .elevation(s.1.elv);
                Some(d)
            },
            None => None
        }
        
    }
    
    /// Sets the method of calculating prayer times.
    ///
    /// Methods can be one of the following:
    /// 
    /// ```no_test
    ///        0: none. Set to default or 0
    ///        1: Egyptian General Authority of Survey
    ///        2: University of Islamic Sciences, Karachi (Shaf'i)
    ///        3: University of Islamic Sciences, Karachi (Hanafi)
    ///        4: Islamic Society of North America
    ///        5: Muslim World League (MWL)
    ///        6: Umm Al-Qurra, Saudi Arabia
    ///        7: Fixed Ishaa Interval (always 90)
    ///        8: Egyptian General Authority of Survey (Egypt)
    ///        9: Umm Al-Qurra Ramadan, Saudi Arabia
    ///       10: Moonsighting Committee Worldwide
    ///       11: Morocco Awqaf, Morocco
    /// ```
    pub fn method(&mut self, id: u8) -> &mut ShamsSpec {
        unsafe {itl::getMethod(id as c_int, &mut self.method)};
        self
    }
    
    /// Sets latitude and longitude locations.
    pub fn coordinates(&mut self, lat: f64, lon: f64) -> &mut ShamsSpec {
        self.location.degree_lat = lat as c_double;
        self.location.degree_long = lon as c_double;
        self
    }

    
    /// Sets the temperature to `celsius`. Temperature values are in 
    /// Celsius.
    pub fn temperature(&mut self, celsius: f64) -> &mut ShamsSpec {
        self.location.temperature = celsius;
        self
    }
    
    /// Sets the atmospheric pressure to `millibar`. Pressure values 
    /// are in millibars.
    pub fn pressure(&mut self, millibar: f64) -> &mut ShamsSpec {
        self.location.pressure = millibar;
        self
    }

    /// Sets the elevation to `meters` meters above sea level.
    pub fn elevation(&mut self, meters: f64) -> &mut ShamsSpec {
        self.location.sea_level = meters;
        self
    }
    
    /// Sets date of interest as today according to local time.
    pub fn today(&mut self) -> &mut ShamsSpec {
        use time;
        let tm = time::now();
        // TM is retarded. tm_year starts from 1900, tm_mon starts from 0
        // and tm_mday starts from 1 ...
        // reminds me of "1"-1 == 0 && "1"+1 == "11".
        self.date.year = tm.tm_year+1900;
        self.date.month = tm.tm_mon+1;
        self.date.day = tm.tm_mday;
        
        self
    }
    /// Sets the date of interest to the date represented by its 
    /// arguments.
    /// 
    /// Ranges are inclusive:
    /// - `day` range is [1, 31]
    /// - `month` range: [1, 12]
    /// - `year` is the common 4 digit representation.
    pub fn date(&mut self, year :u16, month: u16, day: u16) -> &mut ShamsSpec {
        self.date.year = year as c_int;
        self.date.month = month as c_int;
        self.date.day = day as c_int;
        
        self
    
    }
    /// Sets the date to the date defined the Unix timestamp. 
    /// See the top-level documentation for the exact definition 
    /// of timestamp.
    pub fn unix_date(&mut self, ts: i64) -> &mut ShamsSpec {
        use time;
        let t = time::at(time::Timespec::new(ts, 0));
        self.date.year = t.tm_year + 1900 as c_int;
        self.date.month = t.tm_mon + 1 as c_int;
        self.date.day = t.tm_mday as c_int;
        self
    }
    
    
    /// Sets the Fajr angle in degrees. 
    ///
    /// This overrides the value defined by a previous call to `method`.
    pub fn fajr_angle(&mut self, degrees: f64) -> &mut ShamsSpec {
        self.method.fajr_ang = degrees;
        self
    }


    /// Sets the Isha angle in degrees.
    ///
    /// This overrides the value defined by a previous call to `method`.
    pub fn isha_angle(&mut self, degrees: f64) -> &mut ShamsSpec {
        self.method.ishaa_ang = degrees;
        self
    }
    
    
    /// Sets the Fajr interval in minutes between Fajr and Shurooq.
    ///
    /// This overrides the value defined by a previous call to `method`.
    pub fn fajr_interval(&mut self, minutes: i32) -> &mut ShamsSpec {
        self.method.fajr_inv = minutes;
        self
    }
    
    /// Sets the Isha interval in minutes between Magrib and Isha
    ///
    /// This overrides the value defined by a previous call to `method`.
    pub fn isha_interval(&mut self, minutes: i32) -> &mut ShamsSpec {
        self.method.ishaa_inv = minutes;
        self
    }
    
    /// Selects the Asr time calculation method; 1: Shafi, 2: Hanafi.
    ///
    /// This overrides the value defined by a previous call to `method`.
    pub fn aser_mathhab(&mut self, id: i32) -> &mut ShamsSpec {
        self.method.mathhab = id as c_int;
        self
    }
    
    
    /// (used for extreme methods: refer to ITL's documentation)
    pub fn nearest_latitude(&mut self, angle :f64) -> &mut ShamsSpec {
        self.method.nearest_lat = angle;
        self
    }
    
    /// (used for extreme methods: refer to ITL's documentation)
    pub fn extreme(&mut self, extreme :i32) -> &mut ShamsSpec {
        self.method.extreme = extreme as c_int;
        self
    }
    
    /// Calculates the times of interest based on the supplied 
    /// information. The specification object can be reused post
    /// calculation.
    pub fn calculate(&self) -> Result<ShamsDay, String> {
        use ::date::{uts, month_list};
        
        let mut buf: [itl::Prayer; 6] = [Default::default(); 6];
        let mut config = self.clone();
        
        // Validation
        if config.date.year < 1970 {
            return Err(format!(concat!("Minimum year supported is 1970: ",
                    "supplied year is {}."), config.date.year));
        }
        if config.date.month < 1 || config.date.month > 12 {
            return Err(format!(concat!("Invalid month specified: valid ",
                    "month is in the range of [1, 12]. supplied ",
                    "month is {}."), config.date.month));
        }
        if config.date.day < 1 ||
           config.date.day as u16 > month_list(config.date.year as u16)[config.date.month as usize - 1] {
            return Err(format!(concat!("Invalid date specified for month ",
                    "{}: valid day is in the range of [1, {}]. supplied ",
                    "day is {}."), config.date.month, 
                    month_list(config.date.year as u16)[config.date.month as usize - 1],
                    config.date.day));
        
        }
        
        if !(config.location.degree_lat >= -90.000001 && config.location.degree_lat <= 90.000001) {
            return Err(format!(concat!("Invalid latitude specified: valid ",
                    "latitude is in the range of [-90.0, 90.0]. supplied ",
                    "month is {}."), config.location.degree_lat));
        }
        
        if !(config.location.degree_long >= -180.000001 && config.location.degree_long <= 180.000001) {
            return Err(format!(concat!("Invalid longitude specified: valid ",
                    "longitude is in the range of [-180.0, 180.0]. supplied ",
                    "month is {}."), config.location.degree_long));
        }
        
        if config.method.method > 11 {
            return Err(format!(concat!("Invalid method specified: valid ",
                    "method is in the range of [0, 11]. supplied ",
                    "month is {}."), config.method.method));
        }
        
        // convert Prayer time -> Option<timestamp>, extreme
        macro_rules! fill {
            ($i: expr) => (
                if buf[$i].hour == 99 || buf[$i].minute == 99 {(None, false)}
                else {
                    (Some(Time(uts(
                                config.date.year as u16, config.date.month as u16, 
                                config.date.day as u16, buf[$i].hour as u16, 
                                buf[$i].minute  as u16, buf[$i].second as u16))), 
                                buf[$i].is_extreme != 0)}
            )
        }
        
        // fill current day with prayer times for the specified date
        unsafe {itl::getPrayerTimes(&config.location, &config.method, &config.date, buf.as_mut_ptr())};
        let current_day = [fill!(0), fill!(1), 
                          fill!(2), fill!(3), 
                          fill!(4), fill!(5)];

        // jump to the next date by adding 24 hours to the current day, 
        // then fill next_day with prayertime
        let next_day = {
            // satisfying rust's borrow checker
            let year = config.date.year as u16;
            let month = config.date.month as u16;
            let day = config.date.day as u16;
            
            config.unix_date(uts(year, month, day, 12, 0, 0)+24*3600);
            
            unsafe {itl::getPrayerTimes(&config.location, &config.method, &config.date, buf.as_mut_ptr())};
            
            [fill!(0), fill!(1), fill!(2), fill!(3), fill!(4), fill!(5)]
        };
        
        Ok(ShamsDay {
            date: Date {year    : self.date.year as u16, 
                        month   : self.date.month as u16, 
                        day     : self.date.day as u16},
            current: current_day,
            next: next_day
        })
    }
    
    /// Returns an iterator that produces a ShamsDay for each day 
    /// of the specified year.
    pub fn year(&self, year: u16) -> Result<ShamsYear, String> {
        ShamsYear::new(self, year)
    }
}


/// A Collection of times of interest for a specific date
///
/// Some methods will use `Extreme Methods` to preform the required 
/// calculates if configured to do so. See the `Extreme Methods` 
/// section in the top level of the documentation for a description 
/// of what extreme methods are and what they entail.
#[derive(Clone, Copy)]
pub struct ShamsDay {
    date        : Date,
    current     : [(Option<Time>, bool); 6],
    next        : [(Option<Time>, bool); 6]
}
impl ShamsDay {
    
    /// Returns the date for which the results in this object apply.
    pub fn date(&self) -> Date {
        self.date
    }
    
    /// Time of Fajr prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn fajr(&self) -> (Option<Time>, bool) {
        self.current[0]
    }
    /// Time of next-day Fajr prayer if calculable and a 
    /// flag to signal if the time was calculated using extreme 
    /// methods.
    pub fn next_fajr(&self) -> (Option<Time>, bool) {
        self.next[0]
    }
    /// Time of Shurooq prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn shurooq(&self) -> (Option<Time>, bool) {
        self.current[1]
    }
    /// Time of next-day Shurooq prayer if calculable and 
    /// a flag to signal if the time was calculated using extreme 
    /// methods.
    pub fn next_shurooq(&self) -> (Option<Time>, bool) {
        self.next[1]
    }
    /// Time of Zuhr prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn zuhr(&self) -> (Option<Time>, bool) {
        self.current[2]
    }
    /// Time of Asr prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn asr(&self) -> (Option<Time>, bool) {
        self.current[3]
    }
    /// Time of Maghrib prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn maghrib(&self) -> (Option<Time>, bool) {
        self.current[4]
    }
    /// Time of Isha prayer if calculable and a flag to 
    /// signal if the time was calculated using extreme methods.
    pub fn isha(&self) -> (Option<Time>, bool) {
        self.current[5]
    }
    /// Time of Sunrise.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    pub fn sunrise(&self) -> Option<Time> {
        let (t, e) = self.shurooq();
        if e {return None;}
        t
    }
    
    /// Time of next-day sunrise.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    pub fn next_sunrise(&self) -> Option<Time> {
        let (t, e) = self.next_shurooq();
        if e {return None;}
        t
    }
    
    /// Time of Sunset.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    pub fn sunset(&self) -> Option<Time> {
        let (t, e) = self.maghrib();
        if e {return None;}
        t
    }
    
    /// Duration (no leap seconds) of daytime in seconds:
    /// The time between sunrise and sunset.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    pub fn daytime(&self) -> Option<Duration> {
        if let (Some(Time(sr)), Some(Time(ss))) = (self.sunrise(), self.sunset()) {
            Some(Duration(ss - sr))
        }
        else {
            None
        }
    }
    
    /// Dur ration (no leap seconds) of nighttime in seconds:
    /// The time between sunset and sunrise.
    /// 
    /// Returns `None` if this value can not be reliably calculated.
    pub fn nighttime(&self) -> Option<Duration> {
        if let (Some(Time(ss)), Some(Time(nsr))) = (self.sunset(), self.next_sunrise()) {
            Some(Duration(nsr - ss))
        }
        else {
            None
        }
    }
    
    
    /// Returns POSIX time of midday; between sunrise and sunset.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    pub fn noon(&self) -> Option<Time> {
        if let (Some(Duration(d)), Some(Time(sr))) = (self.daytime(), self.sunrise()) {
            Some(Time(sr + d/2))
        }
        else {
            None
        }
    }
    
    
    /// Returns the midpoint between sunset and sunrise.
    ///
    /// Returns `None` if this value can not be reliably calculated.
    /// 
    /// Note that this value does not represent the end of
    /// the Isha time if it ends at "midnight". In Islam, 
    /// daytime starts at dusk, rather than sunrise.
    pub fn midnight(&self) -> Option<Time> {
        if let (Some(Duration(n)), Some(Time(ss))) = (self.nighttime(), self.sunset()) {
            Some(Time(ss + n/2))
        }
        else {
            None
        }
    }
    
    /// Returns a time at which the sun would be perceived to be at
    /// `angle` with the location from which it rose.
    ///
    /// The times this function returns correspond to what humans 
    /// would perceive as the reasonable values.
    /// 
    /// To maintain perception accuracy, `angle` does not linearly 
    /// correlate with time or relative location of the sun.
    /// See `phase_time`.
    /// 
    /// Also, the time at `angle` 0.0 does not necessarily equate to 
    /// that of 60.0. Angles 0.0 and 360.0 correspond to the sunrises 
    /// of the current day and the day the follows it, respectively.
    ///
    /// Angle values must be between 0.0 and 360.0. if this is 
    /// not the case, angle will be clipped to those boundaries.
    /// 
    pub fn angle_time(&self, angle: f64) -> Option<Time> {
        let mut phase = angle;
        if phase < 0.0 { phase = 0.0 }
        if phase > 360.0 { phase = 360.0 }
        phase /= 180.0;
        
        self.phase_time(phase)
    }
    
    /// Returns the POSIX time when the sun is at a phase defined in the
    /// table bellow:
    ///
    /// - Normalized phase of `0.0` implies sunrise.
    /// - Normalized phase of `0.5` implies noon.
    /// - Normalized phase of `1.0` implies sunset.
    /// - Normalized phase of `1.5` implies midnight.
    /// - Normalized phase of `2.0` implies the sunrise of the next day
    ///
    /// Phase value must be between 0.0 and 2.0, inclusive. If this isn't
    /// the case, `phase` will be clipped to the nearest boundary.
    ///
    /// Note: The phase value does not linearly correlate with the sun's 
    /// actual angle (time of day); the duration between 0.0 and 1.0 will 
    /// likely be different than the duration between 1.0 and 2.0, 
    /// depending on the location and time of year.
    pub fn phase_time(&self, phase: f64) -> Option<Time> {
        let mut phase = phase;
        if phase < 0.0 {
            phase = 0.0;
        }
        if phase > 2.0 {
            phase = 2.0;
        }
        if let (Some(Duration(daytime)), Some(Duration(nighttime)), Some(Time(sunrise))) = 
                                                  (self.daytime(), self.nighttime(), self.sunrise()) {
            if phase <= 1.0 {
                Some(Time(
                    sunrise + ((daytime as f64) * phase) as i64
                ))
            }
            else {
                Some(Time(
                    sunrise + daytime + ((nighttime as f64) * (phase - 1.0)) as i64
                ))
            }
        }
        else {
            None
        }
    }
    // copy to Spec for a general on the spot generation?
    pub fn time_phase(&self, time: i64) -> Option<f64> {
        if let (Some(Duration(daytime)), Some(Duration(nighttime)), Some(Time(sunrise))) = 
                                                  (self.daytime(), self.nighttime(), self.sunrise()) {
            let time = time - sunrise;
            
            // if within current self's day/night
            if time <= daytime + nighttime && time >= 0 {
                if time <= daytime {
                    Some(time as f64/daytime as f64)
                }
                else {
                    Some(1.0 + (time-daytime) as f64/nighttime as f64)
                }
            }
            else {
                None
            }
        }
        else {
            None
        }
    }
}


/// An Iterator over every day of a given year, yielding a
/// ShamsDay object for each day. 
/// 
/// See ShamsSpec for generating an instance.
pub struct ShamsYear {
    spec        : ShamsSpec,
    day         : u16,
    month       : u16,
    year        : u16,
    dim: &'static [u16]
}
impl ShamsYear {
    fn new(spec: &ShamsSpec, year: u16) -> Result<ShamsYear, String> {
        use ::date::month_list;
        if let Err(e) = spec.clone().date(year, 1, 1).calculate() {
            return Err(e);
        }
        else {
            Ok(ShamsYear{   dim:month_list(year),
                            spec:*spec,
                            year:year,
                            month:1,
                            day:1               })
        }
    }
}
impl Iterator for ShamsYear {
    type Item=ShamsDay;
    fn next(&mut self) -> Option<ShamsDay> {
        if self.day > self.dim[self.month as usize - 1] {
            self.day = 1;
            self.month += 1;
            if self.month as usize - 1 == self.dim.len() {
                return None;
            }
        }
        self.spec.date(self.year, self.month, self.day);
        self.day += 1;
        Some(self.spec.calculate().unwrap_or_else(|e| 
            panic!(concat!("This is an unreported bug in the ",
                           "library, please report it with a ",
                           "method to reproduce it: {}"), e)))
    }
}


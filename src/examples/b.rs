/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

//! Longest day of the year
//!
//!
//! ```
//! extern crate shams;
//! 
//! fn main() {
//!     use shams::{ShamsSpec, Date, Duration};
//!     use std::default::Default;
//!     
//!
//!     // Get an iterator that yields a ShamsDay object for every day of the 
//!     // year 2003
//!     let mut year = ShamsSpec::city("Makkah")
//!                         .unwrap_or_else(|| panic!("unknown city"))
//!                         .year(2003).unwrap(); 
//!     
//!     // A variable to hold the longest day
//!     let mut max: (Date, Duration) = Default::default();
//!
//!     // iterate of all days and search for longest day
//!     while let Some(day) = year.next() {
//!         if let Some(daytime) = day.daytime() {
//!             if daytime > max.1 { max = (day.date(), daytime) }
//!         }
//!         else {
//!             println!("Ignored {}: Sun doesn't rise or set.", day.date())
//!         }
//!     }
//!
//!     // Destructuring max for better legibility.
//!     let (date, durration) = max;
//!
//!     
//!     println!(concat!("longest day in Makkah for the year {} was on {}, ", 
//!                      "lasting for {} hours."), 
//!                      date.year, date,
//!                      durration.hours());
//!     
//!
//! }
//! ```


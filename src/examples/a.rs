/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

//! The time between Al-Fajr and true midnight
//!
//!
//! ```
//! extern crate shams;
//! 
//! fn main() {
//!     use shams::ShamsSpec;
//! 
//!     // Specify location and calculation details
//!     let day = ShamsSpec::new().coordinates(31.77802, 35.23532)
//!                                   .temperature(17.5).method(1)
//!                                   .elevation(720.00).today()
//!                                   .pressure(1010.0)
//!                                   .calculate().unwrap();
//! 
//!     // Get true midnight and Al-fajr, if posible.
//!     if let (Some(midnight), (Some(fajr), _)) = (day.midnight(), day.fajr()) {
//!         println!("Midnight {} hours after fajr", (midnight - fajr).hours());
//!     }
//! }
//!
//! ```

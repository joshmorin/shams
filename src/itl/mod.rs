/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

use libc::types::os::arch::c95::{c_double, c_int};
use std::default;

#[derive(Default, Copy, Clone, Debug)]
#[repr(C)]
pub struct Date {
    pub day     : c_int,
    pub month   : c_int,
    pub year    : c_int
}

#[derive(Default, Copy, Clone, Debug)]
#[repr(C)]
pub struct Location {
    pub degree_long  : c_double, /* Longitude in decimal degree. */
    pub degree_lat   : c_double, /* Latitude in decimal degree. */
    pub gmt_diff     : c_double, /* GMT difference at regular time. */
    pub dst          : c_int,    /* Daylight savings time switch (0 if not used).
                                    Setting this to 1 should add 1 hour to all the
                                    calculated prayer times */
    pub sea_level    : c_double, /* Height above Sea level in meters */
    pub pressure     : c_double, /* Atmospheric pressure in millibars (the
                                    astronomical standard value is 1010) */
    pub temperature  : c_double  /* Temperature in Celsius degree (the astronomical
                                    standard value is 10) */
}


#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct Method {
    pub method       : c_int,       /* Chosen calculation method */
    pub fajr_ang     : c_double,    /* Fajr angle */
    pub ishaa_ang    : c_double,    /* Ishaa angle */
    pub imsaak_ang   : c_double,    /* The angle difference between Imsaak and Fajr (
                                   default is 1.5)*/
    pub fajr_inv     : c_int,       /* Fajr Interval is the amount of minutes between
                                   Fajr and Shurooq (0 if not used) */
    pub ishaa_inv    : c_int,       /* Ishaa Interval is the amount if minutes between
                                   Ishaa and Maghrib (0 if not used) */
    pub imsaak_inv   : c_int,       /* Imsaak Interval is the amount of minutes between
                                   Imsaak and Fajr. The default is 10 minutes before
                                   Fajr if Fajr Interval is set */
    pub round        : c_int,       /* Method used for rounding seconds:
                                   0: No Rounding. "Prayer.seconds" is set to the
                                      amount of computed seconds.
                                   1: Normal Rounding. If seconds are equal to
                                      30 or above, add 1 minute. Sets
                                      "Prayer.seconds" to zero.
                                   2: Special Rounding. Similar to normal rounding
                                      but we always round down for Shurooq and
                                      Imsaak times. (default)
                                   3: Aggressive Rounding. Similar to Special
                                      Rounding but we add 1 minute if the seconds
                                      value is equal to 1 second or more.  */
    pub mathhab      : c_int,       /* Assr prayer shadow ratio:
                                   1: Shaf'i (default)
                                   2: Hanafi */
    pub nearest_lat  : c_double,    /* Latitude Used for the 'Nearest Latitude' extreme
                                   methods. The default is 48.5 */
    pub extreme      : c_int,       /* Extreme latitude calculation method (see
                                   below) */
    pub extreme_lat  : c_double,    /* Latitude at which the extreme method should
                                   always be used. The default is 55 */
    pub offset       : c_int,       /* Enable Offsets switch (set this to 1 to
                                   activate). This option allows you to add or
                                   subtract any amount of minutes to the daily
                                   computed prayer times based on values (in
                                   minutes) for each prayer in the offList array */
    pub off_list     : [c_double; 6],  /* For Example: If you want to add 30 seconds to
                                   Maghrib and subtract 2 minutes from Ishaa:
                                   offset = 1
                                   offList[4] = 0.5
                                   offList[5] = -2
                                   ..and than call getPrayerTimes as usual. */

}
impl default::Default for Method {
    fn default() -> Method {
        Method { 
            method:0, fajr_ang:0.0, ishaa_ang:0.0, 
            imsaak_ang:0.0, fajr_inv:0, ishaa_inv:0, 
            imsaak_inv:0, round:0, mathhab:0, 
            nearest_lat:0.0, extreme:0, extreme_lat:0.0, 
            offset:0, off_list: [0.0; 6]
        }        
    }
}

#[derive(Default, Copy, Clone, Debug)]
#[repr(C)]
pub struct Prayer {
     pub hour       : c_int,    /* prayer time hour */
     pub minute     : c_int,    /* prayer time minute */
     pub second     : c_int,    /* prayer time second */
     pub is_extreme : c_int     /* Extreme calculation status. The 'getPrayerTimes'
                               function sets this variable to 1 to indicate that
                               this particular prayer time has been calculated
                               through extreme latitude methods and NOT by
                               conventional means of calculation. */
}

// linked with the itl compiled by the build script
//#[link(name = "itl")]
extern {
    pub fn getMethod(n: c_int, conf: *mut Method);
    pub fn getPrayerTimes( loc: *const Location, 
                            met: *const Method, 
                            date: *const Date, 
                            res: *mut Prayer);
}



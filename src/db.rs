/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    -----------------------------------------------------------------
    
    Special Exception for this file: Location-related data in this 
    file (including coordinates, elevation, pressure, temprature, 
    timezone, and method ID) is available for use and redistribution 
    with no restriction. This license is limited to the data only.
*/

pub struct City {
    pub lat     : f64,
    pub lon     : f64,
    pub elv     : f64,
    pub pres    : f64,
    pub temp    : (f64, f64), // min, max
    pub tz      : f64,
    pub method  : u8
}


const SA_MAKKAH:City=City{lat:21.42254,lon:39.82643,elv:430.0,pres:1010.0,temp:(25.0, 36.0),tz:3.0,method:6};
const SA_MEDINA:City=City{lat:24.468334,lon:39.610834,elv:620.0,pres:1010.0,temp:(23.0, 33.0),tz:3.0,method:6};
const PS_ALQUDS:City=City{lat:31.77802,lon:35.23532,elv:720.00,pres:1010.0,temp:(9.0, 25.0),tz:2.0,method:1};
const ID_JAKARTA:City=City{lat:-6.2,lon:106.816667,elv:16.0,pres:1010.0,temp:(26.5, 28.0),tz:7.0,method:1};
const BN_BANDAR:City=City{lat:4.890278,lon:114.942222,elv:70.0,pres:1010.0,temp:(26.5, 28.0),tz:8.0,method:1};
const KW_KUWAIT:City=City{lat:29.3667,lon:47.9667,elv:40.0,pres:1010.0,temp:(13.5, 40.0),tz:3.0,method:2};


// Optimize later if need arises: reorder and use binary search.
// Format ([city name, country name], City object)
pub const CITIES: &'static[(&'static[&'static str], &'static City)] = &[
    /*‎*/(&["Makkah", "Saudi Arabia"], &SA_MAKKAH),
    /*‎*/(&["مكة", "السعودية"], &SA_MAKKAH),
    /*‎*/(&["Medina", "Saudi Arabia"], &SA_MEDINA),
    /*‎*/(&["المدينة المنورة", "السعودية"], &SA_MEDINA),
    /*‎*/(&["Al-Quds", "Palestine"], &PS_ALQUDS),
    /*‎*/(&["القدس", "فلسطين"], &PS_ALQUDS),
    /*‎*/(&["Jakarta", "Indonesia"], &ID_JAKARTA),
    /*‎*/(&["جاكرتا", "إندونيسيا"], &ID_JAKARTA),
    /*‎*/(&["Bandar Seri Begawan", "Brunei"], &BN_BANDAR),
    /*‎*/(&["بندر سري بكاوان", "بروناي"], &BN_BANDAR),
    /*‎*/(&["Kuwait", "Kuwait"], &KW_KUWAIT),
    /*‎*/(&["الكويت", "الكويت"], &KW_KUWAIT),
];



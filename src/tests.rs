/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// Testing accuracy of times
#[test]
fn timing_accuracy() {
    use ::date;
    use ::ShamsSpec;
    
    macro_rules! city_time_check {
        ($city_name:expr, $prayer:ident, $date:expr, $time:expr, $tz: expr) => ({
            
            let calc = ShamsSpec::city($city_name)
                        .unwrap_or_else(|| panic!(concat!("Unknown City: ", $city_name)))
                        .date($date.0, $date.1, $date.2).calculate().unwrap()
                        .$prayer().0.unwrap_or_else(|| panic!(concat!("No ", stringify!($prayer), 
                                                    " defined for ", 
                                                    $city_name)));
                                                    
            let def = (date::uts($date.0, $date.1, $date.2, $time.0, $time.1, $time.2) - $tz*3600) as i64;
            let diff =  calc.0 - def;
            
            // max difference is 5 minutes between specified timings 
            // and library results.
            // 
            // This test is ment to insure that this library isn't 
            // introducing calculation errors to the underlying 
            // calculation methods, and not to insure the accuracy 
            // of the underlying calculation methods themselves.
            if diff.abs() > 300 {
                panic!("diff for {} for {} is {} seconds ({} - {})", 
                    $city_name, stringify!($prayer), diff.abs(), calc.0, def)
            
            }
        })
    }
    
    city_time_check!("Kuwait", shurooq, (2015, 8, 17), (5, 17, 00), 3);
    city_time_check!("Kuwait", zuhr, (2015, 8, 17), (11, 52, 00), 3);
    city_time_check!("Kuwait", asr, (2015, 8, 18), (15, 27, 00), 3);
    city_time_check!("Kuwait", maghrib, (2015, 8, 18), (18, 25, 00), 3);
    city_time_check!("Kuwait", isha, (2015, 8, 18), (19, 48, 00), 3);
    
    city_time_check!("Makkah", shurooq, (2015, 8, 17), (5, 59, 00), 3);
    city_time_check!("Makkah", zuhr, (2015, 8, 17), (12, 25, 00), 3);
    city_time_check!("Makkah", asr, (2015, 8, 17), (15, 48, 00), 3);
    city_time_check!("Makkah", maghrib, (2015, 8, 17), (18, 50, 00), 3);
    city_time_check!("Makkah", isha, (2015, 8, 17), (20, 20, 00), 3);
    
}


// Random tests add through out development
#[test]
fn it_works() {
    use std::default::Default;
    use ::ShamsSpec;
    use ::date;
    
    // Can only test for sanity because the underlying algorithms used by 
    // ITL might not be guaranteed to be stable. 
    let times = ShamsSpec::new().method(6).coordinates(29.366667, 47.966667)
                                      .temperature(25.0)
                                      .pressure(1010.0).elevation(20.0)
                                      .date(2015, 8, 13).calculate().unwrap();
                                      
    assert!(times.sunrise().unwrap() < times.sunset().unwrap());
    assert!(times.sunrise().unwrap() < times.noon().unwrap());
    assert!(times.sunset().unwrap() > times.noon().unwrap());
    assert!(times.sunset().unwrap() < times.midnight().unwrap());
    assert!(times.fajr().0.unwrap() < times.shurooq().0.unwrap());
    assert!(times.shurooq().0.unwrap() < times.zuhr().0.unwrap());
    assert!(times.zuhr().0.unwrap() < times.asr().0.unwrap());
    assert!(times.asr().0.unwrap() < times.maghrib().0.unwrap());
    assert!(times.maghrib().0.unwrap() < times.isha().0.unwrap());
    
    // test cities
    let times_a = ShamsSpec::city("الكويت").unwrap_or_else(|| panic!("Should be defined"))
                                             .today().calculate().unwrap();
    
    let times_b = ShamsSpec::city("Kuwait, Kuwait").unwrap_or_else(|| panic!("Should be defined"))
                                             .today().calculate().unwrap();
    
    assert!(times_a.isha() == times_b.isha());
    
    assert!(ShamsSpec::city("Al-Quds, Palestine").is_some());
    assert!(ShamsSpec::city("Al-Quds Palestine").is_some());
    assert!(ShamsSpec::city("Al-Quds").is_some());
    assert!(ShamsSpec::city("القدس، فلسطين").is_some());
    assert!(ShamsSpec::city("القدس فلسطين").is_some());
    assert!(ShamsSpec::city("القدس").is_some());    
    
    // extreme
    let times = ShamsSpec::new().method(6).coordinates(90.00, 0.1)
                                      .temperature(25.0)
                                      .pressure(1010.0).elevation(20.0)
                                      .date(2015, 8, 13)
                                      .calculate().unwrap();
    
    assert!(times.sunrise() == None);
    assert!(times.sunset() == None);
    assert!(times.noon() == None);
    
    
    // Iterate of all days and search for longest day
    let mut year = ShamsSpec::city("Makkah")
                .unwrap_or_else(|| panic!("unknown city"))
                .year(2003).unwrap(); 
    
    let mut max: (date::Date, date::Duration) = Default::default();
    
    while let Some(day) = year.next() {
        if let Some(daytime) = day.daytime() {
            if daytime > max.1 {
                max = (day.date(), daytime);
            }
        }
        // Ingoring days where the sun doesn't rise or set
    }
    
    // converting unix time to hours, minutes, and seconds
    let (date, durration) = max;

    println!(concat!("longest day in Makkah for the year {} was on {}/{}, ", 
                     "lasting for {} hours."), 
                      date.year, date.day, date.month,
                      durration.hours());
                      
    /* Readme Example */ {
        use ::ShamsSpec;

        // Specify location and calculation details
        let day = ShamsSpec::new().coordinates(31.77802, 35.23532)
                                      .temperature(17.5)
                                      .pressure(1010.0).method(1)
                                      .elevation(720.00).today()
                                      .calculate().unwrap();

        // Get true midnight and Al-fajr, if posible.
        if let (Some(midnight), (Some(fajr), _)) = (day.midnight(), day.fajr()) {
            println!("Midnight {} hours after fajr", (midnight - fajr).hours());
        }

        // Get the time when the sun is preceptually ~20° past sunrise
        if let Some(too_bright) = day.angle_time(20.0) {
            println!("Wear sunglasses at {}", too_bright);
        }
    }
}

#[test]
fn phase() {
    use ::ShamsSpec;
    
    let st = ShamsSpec::city("Medina").unwrap_or_else(|| panic!("this should be known"))
                .today().calculate().unwrap_or_else(|e| panic!("{}", e));
    
    let sunrise = st.sunrise().unwrap_or_else(|| panic!());
    let noon = st.noon().unwrap_or_else(|| panic!());
    let sunset = st.sunset().unwrap_or_else(|| panic!());
    let midnight = st.midnight().unwrap_or_else(|| panic!());
    let next_sunrise = st.next_sunrise().unwrap_or_else(|| panic!());
    
    let sunrise2 = st.phase_time(0.0).unwrap_or_else(|| panic!());
    let noon2 = st.phase_time(0.5).unwrap_or_else(|| panic!());
    let sunset2 = st.phase_time(1.0).unwrap_or_else(|| panic!());
    let midnight2 = st.phase_time(1.5).unwrap_or_else(|| panic!());
    let next_sunrise2 = st.phase_time(2.0).unwrap_or_else(|| panic!());

    let sunrise3 = st.time_phase(sunrise.0).unwrap_or_else(|| panic!());
    let noon3 = st.time_phase(noon.0).unwrap_or_else(|| panic!());
    let sunset3 = st.time_phase(sunset.0).unwrap_or_else(|| panic!());
    let midnight3 = st.time_phase(midnight.0).unwrap_or_else(|| panic!());
    let next_sunrise3 = st.time_phase(next_sunrise.0).unwrap_or_else(|| panic!());
    
    // max two second tolerances
    assert!((sunrise2 - sunrise).abs() < 2);
    assert!((noon2 - noon).abs() < 2);
    assert!((sunset2 - sunset).abs() < 2);
    assert!((midnight2 - midnight).abs() < 2);
    assert!((next_sunrise2 - next_sunrise).abs() < 2);
    
    assert!((sunrise3 - 0.0).abs() < 1e-6);
    assert!((noon3 - 0.5).abs() < 1e-6);
    assert!((sunset3 - 1.0).abs() < 1e-6);
    assert!((midnight3 - 1.5).abs() < 1e-6);
    assert!((next_sunrise3 - 2.0).abs() < 1e-6);
    
    // max phase tolerances is 1e-6
    macro_rules! test_phase_cons {
        ($phase: expr) => ({
            assert!((st.time_phase(st.phase_time($phase).unwrap_or_else(|| panic!()).0)
                            .unwrap_or_else(|| panic!())-$phase).abs() < 1e-6);
        })
    } 
    test_phase_cons!(0.0);
    test_phase_cons!(0.25);
    test_phase_cons!(0.5);
    test_phase_cons!(0.75);
    test_phase_cons!(1.0);
    test_phase_cons!(1.25);
    test_phase_cons!(1.5);
    test_phase_cons!(1.75);
    test_phase_cons!(2.0);
    
    
}


#[test]
fn basic() {
    /*
    use time;
    use ::ShamsSpec;
    let day = ShamsSpec::city("Kuwait").expect("should be defined")
                .today().calculate().ok().expect("could not calculate");
    println!("
        Kuwait:     {}
        
        fajr:       {}
        shurooq:    {}
        zuhr:       {}
        asr:        {}
        maghrib:    {}
        isha:       {}
        ",
        time::at_utc(time::Timespec::new(time::now().to_timespec().sec, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.fajr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.shurooq().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.zuhr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.asr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.maghrib().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.isha().0.unwrap().0, 0)).rfc3339()
    );
    let day = ShamsSpec::city("Makkah").expect("should be defined")
                .today().calculate().ok().expect("could not calculate");
    println!("
        Makkah:     {}
        
        fajr:       {}
        shurooq:    {}
        zuhr:       {}
        asr:        {}
        maghrib:    {}
        isha:       {}
        ",
        time::at_utc(time::Timespec::new(time::now().to_timespec().sec+2*3600, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.fajr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.shurooq().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.zuhr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.asr().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.maghrib().0.unwrap().0, 0)).rfc3339(),
        time::at_utc(time::Timespec::new(day.isha().0.unwrap().0, 0)).rfc3339()
    );
    */
}

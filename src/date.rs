/*
    This file is a part of the Shams Library.
    
    Shams: Islamic prayer times and other solar temporal events
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
use std::{ops, cmp, fmt};

/// A unit structure to hold the year, month, and day expressed
/// as 16-bit unsigned integers.
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Date {
    pub year: u16,
    pub month: u16,
    pub day: u16
}
impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        use std::io::Write;
        write!(f, "{}-{:02}-{:02}", self.year, self.month, self.day)
    }
}

/// A unit `i64` wrapper that represents Unix time.
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Time(pub i64);
impl Time {pub fn unwrap(self) -> i64 {self.0}}
impl ops::Deref for Time {type Target = i64; fn deref(&self) -> &i64 {&self.0}}
impl cmp::PartialOrd for Time {
    fn partial_cmp(&self, other: &Time) -> Option<cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
impl ops::Sub for Time {
    type Output = Duration;
    fn sub(self, rhs: Time) -> Self::Output {
        Duration(self.0 - rhs.0)
    }
}
impl ops::Sub<i64> for Time {
    type Output = Duration;
    fn sub(self, rhs: i64) -> Self::Output {
        Duration(self.0 - rhs)
    }
}
impl ops::Add<Duration> for Time {
    type Output = Time;
    fn add(self, rhs: Duration) -> Self::Output {
        Time(self.0 + rhs.0)
    }
}
impl fmt::Display for Time {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        use time;
        use std::io::Write;
        write!(f, "{}", time::at_utc(time::Timespec::new(self.0, 0)).rfc3339())
    }
}



/// A unit `i64` wrapper that represents duration expressed using Unix time.
#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Duration(pub i64);
impl Duration {pub fn unwrap(self) -> i64 {self.0}}
impl ops::Deref for Duration {type Target = i64; fn deref(&self) -> &i64 {&self.0}}
impl cmp::PartialOrd for Duration {
    fn partial_cmp(&self, other: &Duration) -> Option<cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
impl cmp::PartialEq<i64> for Duration {
    fn eq(&self, other: &i64) -> bool {
        self.0.eq(other)
    }
}
impl cmp::PartialOrd<i64> for Duration {
    fn partial_cmp(&self, other: &i64) -> Option<cmp::Ordering> {
        self.0.partial_cmp(other)
    }
}
impl ops::Div<i64> for Duration {
    type Output = Duration;
    fn div(self, rhs: i64) -> Self::Output {
        Duration(self.0/rhs)
    }
}
impl fmt::Display for Duration {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        <i64 as fmt::Display>::fmt(&self.0, f)
    }
}
impl Duration {
    pub fn weeks(&self) -> i64 {
        self.0/3600*24*7
    }
    pub fn days(&self) -> i64 {
        self.0/3600*24
    }
    pub fn hours(&self) -> i64 {
        self.0/3600
    }
    pub fn minutes(&self) -> i64 {
        self.0/60
    }
    pub fn seconds(&self) -> i64 {
        self.0
    }
    /// Decomposes duration into (hours, minutes, seconds).
    pub fn clock(&self) -> (i64, i64, i64) {
        let &Duration(seconds) = self;
        let hours = seconds/3600;
        let seconds = seconds%3600;
        let minutes = seconds/60;
        let seconds = seconds%60;
        (hours, minutes, seconds)
    }
}


pub fn month_list(year: u16) -> &'static [u16] {
    const COMMON:&'static [u16] = &[31,28,31,30,31,30,31,31,30,31,30,31];
    const LEAP:&'static [u16] = &[31,29,31,30,31,30,31,31,30,31,30,31];
    if year%4 != 0 {COMMON} 
    else if year%100 != 0 {LEAP}
    else if year%400 != 0 {COMMON}
    else {LEAP}
}

pub fn uts(year: u16, month: u16, day: u16, hour: u16, min: u16, sec: u16) -> i64 {
    // The Open Group Base Specifications Issue 6; IEEE Std 1003.1, 2004 Edition
    let mlist = month_list(year as u16);
    //checks here
    assert!(month >= 1 && month <= 12);
    assert!(day >= 1 && day <= mlist[month as usize - 1]);
    assert!(year >= 1970);
    assert!(hour < 24);
    assert!(min <= 60);
    assert!(sec <= 59);
    
    let year = year - 1900;
    
    let yday = mlist.iter().take((month-1) as usize).fold(0, |acc, n| acc+(*n)) 
               + (day - 1);
    
    let sec = sec as i64;
    let min = min as i64;
    let hour = hour as i64;
    let yday = yday as i64;
    let year = year as i64;
    
    sec + min*60 + hour*3600 + yday*86400 +
    (year-70)*31536000 + ((year-69)/4)*86400 -
    ((year-1)/100)*86400 + ((year+299)/400)*86400
}

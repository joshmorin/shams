Shams: Islamic prayer times and other solar-bound temporal events
=================================================================

Shams is a small library for calculating times that are Dependant 
on the Sun's location throughout the day (and night). This includes the 
ability to calculate Prayer times as well as calculating the times of 
sunrise, sunset, true midnight, and the amount of daytime or nighttime 
for a given day, and the point in time when the sun will be at a 
specified angle.

This library embeds the excellent and prevalent ITL library by the 
Arabeyes project.


# Examples #
The following is an example of how this library can be used:

    use shams::ShamsSpec;

    // Specify location and calculation details
    let day = ShamsSpec::new().coordinates(31.77802, 35.23532)
                              .temperature(17.5).method(1)
                              .elevation(720.00).today()
                              .pressure(1010.0)
                              .calculate()
                              .unwrap();

    // Get true midnight and Al-fajr, if posible.
    if let (Some(midnight), (Some(fajr), _)) = (day.midnight(), day.fajr()) {
        println!("Midnight {} hours after fajr", (midnight - fajr).hours());
    }

    // Get the time when the sun is preceptually ~20° past sunrise
    if let Some(too_bright) = day.angle_time(20.0) {
        println!("Wear sunglasses at {}", too_bright);
    }

If the library has all the necessary information about a given city, 
initialization can be simplified. `st` in both examples are equivalent:
        
    let st = ShamsSpec::city("Al-Quds").expect("Unsupported City")
                                       .today().calculate();
    
Examples: 
- [Simple program](https://joshmorin.github.io/documentation/shams/examples/a/index.html)
- [Calculating the longest day of the year](https://joshmorin.github.io/documentation/shams/examples/b/index.html)
- [(more)](https://joshmorin.github.io/documentation/shams/examples/index.html)

# Building #
This project makes use of Rust's standard build system. If not 
available at crates.io (Currently, it isn't), you can add a git 
dependency to your project:

    [dependencies.shams]
    name = "shams"
    git = "https://joshmorin@bitbucket.org/joshmorin/shams.git"


# Documentation #

Online [API](https://joshmorin.github.io/documentation/shams) reference. 

To insure that the documentation matches the library version you are using, 
consider generating the documentation using cargo.


# Bugs
Please submit an issue to the projects issue tracker at: 
https://bitbucket.org/joshmorin/shams/issues/new

**ANONYMOUS SUBMISSIONS ARE ALLOWED**. Alternatively, You may also 
email me at JoshMorin@gmx.com (Please prefix the subject with 
"BUG").



# Contribution 
For contributing to this library, please submit an issue before starting. 
You may also email me at JoshMorin@gmx.com. *I can not guarantee accepting 
a patch if an accompanying bug report is not submitted before hand.* 

Also open an issue for adding a city to the city database.

    
# Copyright #
Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
